# Angular Mobile

## Online Demo

[https://cerebralfix-174807.appspot.com/](https://cerebralfix-174807.appspot.com/)

## Spent hours
 - 8 hours

## Setup and Run

### Clone this project

### Setup
```
yarn install
```
OR
```
npm install
```

### Run
```
npm start
```

### Build
```
npm run build
```

## STEPS
 - Added Angular4
 - Added Material UI
 - Added Router
 - Added Games Module
 - Added Services
 - Added Core Functions
 - Added In Memory Service
 - Added Messages Between Modules by Service
 - Deployed by Google Cloud Servcie

## RESEARCH HISTORY
### Material design
 - https://material.angular.io/

### Add google icons
```
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

### Add router 
 - https://angular.io/tutorial/toh-pt5
 - https://github.com/AngularClass/angular-starter/issues/1004

### Card with image
 - https://material.angular.io/components/card/examples
 
### ngModel
 - https://stackoverflow.com/questions/38892771/cant-bind-to-ngmodel-since-it-isnt-a-known-property-of-input

### Angular local storage servcie
 - https://stackoverflow.com/questions/40589730/local-storage-in-angular-2
 - https://www.npmjs.com/package/angular-2-local-storage

### Angular4 ngSrc
 - https://stackoverflow.com/questions/37965667/whats-equivalent-to-ngsrc-in-angular2

### Fixed navbar
 - https://github.com/angular/material2/issues/735

### Message between modules
 - https://angular.io/guide/component-interaction
 - https://stackoverflow.com/questions/39954785/different-module-components-communication-in-angular-2
 - https://www.lucidchart.com/techblog/2016/11/08/angular-2-and-observables-data-sharing-in-a-multi-view-application/

### Router back
 - https://www.thepolyglotdeveloper.com/2016/11/navigating-web-application-angular-2-router/

### Deploy Google cloud
 - https://jaykhimani.blogspot.co.nz/2016/10/deploying-angular-2-app-with-angular.html
