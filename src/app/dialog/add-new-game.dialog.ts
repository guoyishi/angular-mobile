import { Component } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';

@Component({
  selector: 'dialog-add-new-game',
  templateUrl: 'add-new-game.dialog.html',
  styleUrls: ['add-new-game.dialog.css']
})
export class AddNewGameDialogComponent {
  game = {
    title: 'Casino Heist',
    imageUrl: 'https://cerebralfix.com/files/large/casinoheist-site-promo1.png',
  }

  constructor(public dialogRef: MdDialogRef<AddNewGameDialogComponent>) {}
}
