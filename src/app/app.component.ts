import { Component } from '@angular/core';
import { MdDialog } from '@angular/material';

import { AddNewGameDialogComponent } from './dialog/add-new-game.dialog';
import { GameService } from './services/game.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(
    public dialog: MdDialog,
    private service: GameService,
  ) { };

  addNewGame() {
    const dialogRef = this.dialog.open(AddNewGameDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.create(result)
          .then(hero => {
            this.service.pleaseUpdate();
          });
      }
    });
  }
}
