import { Component, OnInit } from '@angular/core';

import { MdSnackBar } from '@angular/material';

import { GameService } from '../services/game.service';
import { Game } from '../models/game';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  games: Game[];

  constructor(
    private service: GameService,
    public snackBar: MdSnackBar,
  ) {
    this.service.announced$.subscribe(
      () => {
        this.updateList();
      }
    )
  };

  ngOnInit() {
    this.updateList();
  }

  cardClick(game: Game): void {
    this.snackBar.open(`Game <${game.title}> Clicked!`, null, {
      duration: 1000,
    });
  };

  updateList() {
    this.service.getGames()
      .then(games => this.games = games)
      .then(() => {
        this.snackBar.open(`List Updated!`, null, {
          duration: 1000,
        });
      })
  }
}
