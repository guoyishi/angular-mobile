import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { GameDetailComponent } from './detail/game-detail.component';

const dashboardRoutes: Routes = [
  { path: 'games',  component: DashboardComponent },
  { path: 'game/:id', component: GameDetailComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(dashboardRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule { }
