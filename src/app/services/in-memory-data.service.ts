import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const games = [
      {
        id: 1,
        title: 'Attack of the Cones',
        imageUrl: 'https://cerebralfix.com/files/large/aotc-site-promo3.png',
      },
      {
        id: 2,
        title: 'Ninja Tobu',
        imageUrl: 'https://cerebralfix.com/files/large/nt-site-promo11.png',
      },
      {
        id: 3,
        title: 'Beat Stack',
        imageUrl: 'https://cerebralfix.com/files/large/beatstack_websiteimage11.png',
      },
      {
        id: 4,
        title: 'Terrapets',
        imageUrl: 'https://cerebralfix.com/files/large/terrapets-site-promo2.png',
      },
      {
        id: 5,
        title: 'Jane Austen Unbound',
        imageUrl: 'https://cerebralfix.com/files/large/ja_sitepromo_1l.png',
      },
      {
        id: 6,
        title: 'Magical Math World',
        imageUrl: 'https://cerebralfix.com/files/large/di-ds_sitepromo_l1.png',
      },
      {
        id: 7,
        title: 'Haunted Suburb',
        imageUrl: 'https://cerebralfix.com/files/large/hs_sitepromo_13.png',
      },
    ];
    return { games };
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
