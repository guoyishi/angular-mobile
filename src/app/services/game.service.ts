import { Injectable } from '@angular/core';
// import { LocalStorageService } from 'angular-2-local-storage';
import { Headers, Http } from '@angular/http';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/toPromise';

import { Game } from '../models/game';

@Injectable()
export class GameService {
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private gamesUrl = 'api/games';  // URL to web api

  private announcedSource = new Subject();
  announced$ = this.announcedSource.asObservable();

  constructor(private http: Http) { }

  getGames(): Promise<Game[]> {
    return this.http.get(this.gamesUrl)
      .toPromise()
      .then(response => response.json().data as Game[])
      .catch(this.handleError);
  }


  getGame(id: number): Promise<Game> {
    const url = `${this.gamesUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Game)
      .catch(this.handleError);
  }

  create(game: Game): Promise<Game> {
    return this.http
      .post(this.gamesUrl, JSON.stringify(game), { headers: this.headers })
      .toPromise()
      .then(res => res.json().data as Game)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.gamesUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  pleaseUpdate() {
    this.announcedSource.next();
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
