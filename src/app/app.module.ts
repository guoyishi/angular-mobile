import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { HttpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// local storage
import { LocalStorageModule } from 'angular-2-local-storage';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { DashboardModule } from './dashboard/dashboard.module';

import { AddNewGameDialogComponent } from './dialog/add-new-game.dialog';

import { GameService } from './services/game.service';

@NgModule({
  declarations: [
    AppComponent,
    AddNewGameDialogComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    DashboardModule,
    HttpModule,

    AppRoutingModule,

    // local storage
    LocalStorageModule.withConfig({
      prefix: 'cf',
      storageType: 'localStorage'
    }),
    InMemoryWebApiModule.forRoot(InMemoryDataService),
  ],
  entryComponents: [
    AddNewGameDialogComponent,
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
