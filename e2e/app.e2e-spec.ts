import { AngularMobilePage } from './app.po';

describe('angular-mobile App', () => {
  let page: AngularMobilePage;

  beforeEach(() => {
    page = new AngularMobilePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
